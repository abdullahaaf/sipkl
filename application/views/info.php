<!DOCTYPE html>
	<html>
		<head>
		</head>
		<body>
		<div id="tatalaporan">
			<div class="card">
				<div class="card-block">
					<!-- header -->
					<h3><center><strong>KAIDAH PENULISAN LAPORAN PKLI</strong></center></h3>
					<br>
					<div class="container">
						<p>a. Menggunakan Bahasa Indonesia yang baku dan sesuai dengan ejaan yang telah disempurnakaan</p><br>
						<p>b. Naskah yang dibuat dari kertas HVS 70 gram dan tidak bolak balik</p>
					</div>
				</div>
			</div>
		</div>
		<div id="isilaporan">
			<div class="card">
				<div class="card-block">
					<!-- header -->
					<h3><center><strong>ISI LAPORAN</strong></center></h3>
					<br>
					<div class="container">
						<p>BAB I : PENDAHULUAN</p><br>
						<p>BAB II : KONDISI OBYEKTIF LOKASI PKLI</p>
					</div>
				</div>
			</div>
		</div>

<!-- animasi -->
<script type="text/javascript">
	$('#tatalaporan').addClass('animated bounceInUp');
	$('#isilaporan').addClass('animated bounceInDown');
</script>
		</body>
</html>