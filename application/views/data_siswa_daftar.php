<div class="card" id="cekstatus">
	<div class="card-block">
		<div class="row">
			<h3><center>Data Peserta PKLI Teknik Informatika UIN Malang</center></h3>
		</div>
	<br>
	<br>
	<div class="row">
		<div class="table-responsive">
			<table id="table-rekap" class="table table-bordered table-hover" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Instansi / Perusahaan</th>						
						<th>Tanggal Mulai</th>
						<th>Tanggal Selesai</th>						
  					<th>Status</th>
            <th>Surat</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</div>

<script type="text/javascript">
  $('#cekstatus').addClass('animated bounceInUp');
</script>


<script type="text/javascript">

    var save_method; //for save method string
    var table_pesan_masuk;
    $(document).ready(function() {  
        table_pesan_masuk = $('#table-rekap').dataTable({
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Cdatasiswadaftar/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });
  </script>