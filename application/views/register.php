<head>
			<div id="judul">
				<center><h3>Form Pendaftaran Kelompok PKLI</h3></center>
				<h5><center>Lakukan pengisian form dibawah ini untuk melakukan pendaftaran PKLI </center></h5>

			</div>

</head>
<br>
<form action="<?php echo site_url('Cregister/daftar'); ?>" method="post" >
<div id="datakelompok">
	<div class="card">
	<div class="card-block">
		<div class="container">
			<h3><center><strong>FORM DATA KELOMPOK</strong></center></h3>
			<?php echo $error; ?>
			<br>
			<div class="row">
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user prefix"></i>
						<input type="text" id="anggota1" name="anggota1" class="form-control">
						<label for="anggota1">Nama Anggota</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user-md prefix"></i>
						<input type="text" id="nim1" name="nim1" class="form-control">
						<label for="jurusan">Nim</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-mobile-phone prefix"></i>
						<input type="text" id="telpon1" name="telpon1" class="form-control">
						<label for="jurusan">Nomor Telepon</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user prefix"></i>
						<input type="text" id="anggota2" name="anggota2" class="form-control">
						<label for="anggota1">Nama Anggota</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user-md prefix"></i>
						<input type="text" id="nim2" name="nim2" class="form-control">
						<label for="jurusan">Nim</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-mobile-phone prefix"></i>
						<input type="text" id="telpon2" name="telpon2" class="form-control">
						<label for="jurusan">Nomor Telepon</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user prefix"></i>
						<input type="text" id="anggota3" name="anggota3" class="form-control">
						<label for="anggota1">Nama Anggota</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user-md prefix"></i>
						<input type="text" id="nim3" name="nim3" class="form-control">
						<label for="jurusan">Nim</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-mobile-phone prefix"></i>
						<input type="text" id="telpon3" name="telpon3" class="form-control">
						<label for="jurusan">Nomor Telepon</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user prefix"></i>
						<input type="text" id="anggota4" name="anggota4" class="form-control">
						<label for="anggota1">Nama Anggota</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user-md prefix"></i>
						<input type="text" id="nim4" name="nim4" class="form-control">
						<label for="jurusan">Nim</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-mobile-phone prefix"></i>
						<input type="text" id="telpon4" name="telpon4" class="form-control">
						<label for="jurusan">Nomor Telepon</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user prefix"></i>
						<input type="text" id="anggota5" name="anggota5" class="form-control">
						<label for="anggota1">Nama Anggota</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-user-md prefix"></i>
						<input type="text" id="nim5" name="nim5" class="form-control">
						<label for="jurusan">Nim</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="md-form">
						<i class="fa fa-mobile-phone prefix"></i>
						<input type="text" id="telpon5" name="telpon5" class="form-control">
						<label for="jurusan">Nomor Telepon</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="card">
	<div class="card-block">
		<h3><center><strong>FORM DATA TEMPAT dan WAKTU PKL</strong></center></h3>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="md-form">
					<i class="fa fa-building prefix"></i>
					<input type="text" id="namaInstansi" name="namaInstansi" class="form-control">
					<label for="namaInstansi">Nama Instansi / Perusahaan</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="md-form">
					<i class="fa fa-home prefix"></i>
					<textarea type = "text" class="md-textarea form-control" name="alamat" id="alamat"></textarea>
					<label for="alamat">Alamat Perusahaan / Instansi</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="md-form">
					<i class="fa fa-calendar prefix"></i>
					<input type="text" id="tanggalMulai" name="tanggalMulai" class="form-control">
					<label for="tanggalMulai">Tanggal Mulai PKLI</label>
				</div>
					<script type="text/javascript">
                        $(function(){
                          $('#tanggalMulai').datepicker({
                            format: 'yyyy/mm/dd',
                            autoclose: true,
                            todayHighlight: true
                          });
                        });
                     </script>
			</div>
			<div class="col-md-6">
				<div class="md-form">
					<i class="fa fa-calendar prefix"></i>
					<input type="text" id="tanggalSelesai" name="tanggalSelesai" class="form-control">
					<label for="tanggalSelesai">Tanggal Selesai PKLI</label>
				</div>
					<script type="text/javascript">
                        $(function(){
                          $('#tanggalSelesai').datepicker({
                            format: 'yyyy/mm/dd',
                            autoclose: true,
                            todayHighlight: true
                          });
                        });
                     </script>
			</div>
		</div>
	</div>	
</div>
<center>
	<button class="btn btn-success"><i class="fa fa-plus-square"></i> Simpan</button>
</center>
</form>

<script type="text/javascript">
	$('#datakelompok').addClass('animated bounceInUp');
	$('#judul').addClass('animated bounceInLeft');
</script>