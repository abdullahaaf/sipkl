<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdatasiswadaftar extends CI_Model {

	private function _get_datatables_query($nim)
	{ 
		$this->db->from('kelompok');
		$this->db->join('instansi','instansi.no=kelompok.no','left');
		$this->db->join('peserta','peserta.nim=kelompok.nim','left');
		$this->db->order_by('idKel','desc');

		$i = 0;
	}

	function get_datatables($nim)
	{
		$this->_get_datatables_query($nim);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($nim)
	{
		$this->_get_datatables_query($nim);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from('kelompok');
		return $this->db->count_all_results();
	}
}