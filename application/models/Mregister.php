<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mregister extends CI_Model {

	public function cekNim($nim)
    {
        $this->db->select('nim');
        $this->db->from('peserta');
        $this->db->where('nim',$nim);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function cekNimKel($nim)
    {
        $this->db->from('kelompok');
        $this->db->join('instansi','instansi.no=kelompok.no','left');
        $this->db->where('nim',$nim);
        $query = $this->db->get();
        return $query->row();
    }

    function daftarPeserta($data){
        $this->db->insert('peserta', $data);
        return $this->db->insert_id();
    }

    function daftarInstansi($data){
        $this->db->insert('instansi', $data);
        return $this->db->insert_id();
    }

    function maxInstansi(){
        $this->db->select('MAX(no) as no');
        $this->db->from('instansi');
        $query = $this->db->get();
        return $query->row();
    }

    function daftarKelompok($data)
    {
        $this->db->insert('kelompok', $data);
        return $this->db->insert_id();
    }

}