<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdatapkli extends CI_Model {

	private function _get_datatables_query()
	{ 
		$this->db->from('kelompok');
		$this->db->join('instansi','instansi.no=kelompok.no','left');
		$this->db->join('peserta','peserta.nim=kelompok.nim','left');
		$this->db->order_by('idKel','desc');

		$i = 0;
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from('kelompok');
		return $this->db->count_all_results();
	}

	function ambilNim($nim)
	{
		$this->db->from('peserta');
		$this->db->where('nim',$nim);
		$query = $this->db->get();
		return $query->result();
	}

	public function update($where, $data)
	{
		$this->db->update('instansi', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_kel($idKel)
	{
		$this->db->where('idKel', $idKel);
		$this->db->delete('kelompok');
	}
	
	public function delete_ins($no)
	{
		$this->db->where('no', $no);
		$this->db->delete('instansi');
	}

	public function get_by_id($nim)
	{
		$this->db->from('kelompok');
		$this->db->join('instansi','instansi.no=kelompok.no','left');
		$this->db->where('nim',$nim);
		$this->db->order_by('idKel','desc');
		$query = $this->db->get();

		return $query->row();
	}
}