<?php
class Logout extends CI_Controller {

    function __construct() {
        parent::__construct();
        
    }
    
    function logout() {
//        destroy session
        $this->session->sess_destroy();
        
//        redirect ke halaman login
        redirect(site_url('Cauth'));
    }
}