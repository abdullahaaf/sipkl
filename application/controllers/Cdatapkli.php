<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdatapkli extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sikus')<>1) {
                redirect(site_url('Cauth'));
            }
        $this->load->model('Mdatapkli','model');
    }

    public function index()
    {
    	$this->load->view('header');
        $this->load->view('data_pkli');        
        $this->load->view('footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
            $ni = $this->model->ambilNim($model->nim);
            $namaNim = "";
            foreach ($ni as $key) {
                $namaNim .= $key->nim." ".$key->nama."<br>";
            }
            $row[] = $no;
            $row[] = $model->namaInstansi;
            $row[] = $namaNim;
            $row[] = $model->alamat;
            $row[] = $model->tanggalMulai;
            $row[] = $model->tanggalSelesai;
            if($model->statusPkl=="0")
            {
                $row[] = '<strong><span class="text-danger">Ditolak</span></strong>';
            }elseif($model->statusPkl==1)
            {
                $row[] = '<strong><span class="text-success">Disetujui</span></strong>';
            }else
            {
                $row[] = '<strong><span class="text-warning">Belum Disetujui</span></strong>';
            }

            if($model->statusSurat==1)
            {
                $row[] = '<strong><span class="text-success">Surat Sudah Jadi</span></strong>';
            }elseif ($model->statusSurat==0) {
                $row[] = '<strong><span class="text-danger">Surat Belum Jadi</span></strong>';
            }else
            {
                $row[] = '<strong><span class="text-danger">Surat Belum Jadi</span></strong>';
            }
            $row[] = '
                  <a href="javascript:void_namabyr()" title="Setujui" onclick="update_pkl(1,'."'".$model->no."'".')"><i class="fa fa-check text-success"></i></a><br>
                  <a href="javascript:void_namabyr()" title="Tolak" onclick="update_pkl(0,'."'".$model->no."'".')"><i class="fa fa-close text-danger"></i></a>';
            $row[] = '
                  <a href="javascript:void_namabyr()" title="Jadi" onclick="update_surat(1,'."'".$model->no."'".')"><i class="fa fa-check text-success"></i></a><br>
                  <a href="javascript:void_namabyr()" title="Belum Jadi" onclick="update_surat(0,'."'".$model->no."'".')"><i class="fa fa-close text-danger"></i></a>';            
            $row[] = '
                  <a href="javascript:void_namabyr()" title="Hapus" onclick="delete_data('."'".$model->idKel."'".','."'".$model->no."'".')"><i class="fa fa-trash text-danger"></i></a>';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_update_pkl($statusPkl,$no)
    {
        $data = array(
                'statusPkl' => $statusPkl
            );
        $this->model->update(array('no' => $no), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update_surat($statusSurat,$no)
    {
        $data = array(
                'statusSurat' => $statusSurat
            );
        $this->model->update(array('no' => $no), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($idKel,$no)
    {
        $this->model->delete_kel($idKel);
        $this->model->delete_ins($no);
        echo json_encode(array("status" => TRUE));
    }
}