<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cinfo extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sikus')<>1) {
                redirect(site_url('Cauth'));
            }
    }

    public function index()
    {
    	$this->load->view('header');
        $this->load->view('info');        
        $this->load->view('footer');
    }
}