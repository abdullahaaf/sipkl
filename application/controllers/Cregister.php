<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cregister extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sikus')<>1) {
                redirect(site_url('Cauth'));
            }
        $this->load->model('Mregister','model');
    }

    public function index($error = NULL)
    {
        $data = array(
            'error' => $error
        );
    	$this->load->view('header');
        $this->load->view('register',$data);        
        $this->load->view('footer');
    }

    function daftar()
    {
        for ($i=1; $i <= 5; $i++) { 
            if($this->input->post('nim'.$i)!='')
            {
                $cek = $this->model->cekNim($this->input->post('nim'.$i));
                $cekKel = $this->model->cekNimKel($this->input->post('nim'.$i))->statusPkl;
                if($cekKel != 2){
                    if($cek != 1){
                    $data2 = array(
                        'nim' => $this->input->post('nim'.$i),
                        'nama' => $this->input->post('anggota'.$i),
                        'telepon' => $this->input->post('telpon'.$i),
                        'tanggalMulai' => $this->input->post('tanggalMulai'),
                        'tanggalSelesai' => $this->input->post('tanggalSelesai')
                        );
                        $insert2 = $this->model->daftarPeserta($data2);
                }
                if ($i==1) {
                    $data = array(
                'namaInstansi' => $this->input->post('namaInstansi'),
                'alamat' => $this->input->post('alamat'),
                'statusPkl' => '2'
                );
                $insert = $this->model->daftarInstansi($data);
            }
        $idIntansi = $this->model->maxInstansi()->no;
        for ($i=1; $i <=5 ; $i++) { 
            if($this->input->post('nim'.$i)!='')
            {
                $data3 = array(
                'nim' => $this->input->post('nim'.$i),
                'no' => $idIntansi
                );
                $insert3 = $this->model->daftarKelompok($data3); 
            }  
        }
        $error1 = '<div class="alert alert-error text-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong><center><font color="red">Succes!</center></strong></div>';
            $this->index($error1);
        } else {
            $error = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong><center><font color="red">Warning!</center></strong><center>NIM : '.$this->input->post('nim'.$i).' sudah terdaftar</font></center></div>';
            $this->index($error);
        }
            }
        
        }
    }
}