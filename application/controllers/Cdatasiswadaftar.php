<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdatasiswadaftar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sikus')<>1) {
                redirect(site_url('Cauth'));
            }
        $this->load->model('Mdatasiswadaftar','model');
    }

    public function index()
    {
    	$this->load->view('header');
        $this->load->view('data_siswa_daftar');        
        $this->load->view('footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables($this->session->userdata('username'));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $model->namaInstansi;
            $row[] = $model->tanggalMulai;
            $row[] = $model->tanggalSelesai;
            if($model->statusPkl=="0")
            {
                $row[] = '<strong><span class="text-danger">Ditolak</span></strong>';
            }elseif($model->statusPkl==1)
            {
                $row[] = '<strong><span class="text-success">Disetujui</span></strong>';
            }else
            {
                $row[] = '<strong><span class="text-warning">Belum Disetujui</span></strong>';
            }

            if($model->statusSurat==1)
            {
                $row[] = '<strong><span class="text-success">Surat Sudah Jadi</span></strong>';
            }elseif ($model->statusSurat==0) {
                $row[] = '<strong><span class="text-danger">Surat Belum Jadi</span></strong>';
            }else
            {
                $row[] = '<strong><span class="text-danger">Surat Belum Jadi</span></strong>';
            }
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered($this->session->userdata('username')),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
}