<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sikus')<>1) {
                redirect(site_url('Cauth'));
            }
        $this->load->model('Mdatapkli','model');
    }

    public function index()
    {
    	$this->load->view('header');
        $this->load->view('Vdashboard');        
        $this->load->view('footer');
    }

    public function cek()
    {
        $data = $this->model->get_by_id($this->session->userdata('username'));
        echo json_encode($data);
    }
}